<?php

namespace tests\models;

use app\models\CloudBag;
use Yii;

class CloudBagTest extends \Codeception\Test\Unit
{
    public function testAuthSuccess()
    {
        $dropbox = new CloudBag();
        $result = $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $this->assertEquals(Yii::$app->params['accountId'], $result['account_id']);
    }

    public function testAuthWrongAccessToken()
    {
        $dropbox = new CloudBag();
        $result = $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'],
            'wrongAccessToken');

        $this->assertEquals(false, $result);
    }

    public function testOpenFolderErrorGettingData()
    {
        $dropbox = new CloudBag();
        $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $result = $dropbox->openFolder('/wrongPath');

        $this->assertEquals(false, $result);
    }

    public function testDeleteFileSuccess()
    {
        $dropbox = new CloudBag();
        $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $result = $dropbox->delete('/tempTest/testFile.txt');

        $this->assertEquals('testFile.txt', $result);
    }

    public function testDeleteFileWrongPath()
    {
        $dropbox = new CloudBag();
        $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $result = $dropbox->delete('/tempTest/wrongPathFile.txt');

        $this->assertEquals(false, $result);
    }

    public function testDeleteFolderSuccess()
    {
        $dropbox = new CloudBag();
        $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $result = $dropbox->delete('/tempTest/testFolder');

        $this->assertEquals('testFolder', $result);
    }

    public function testDeleteFolderWrongPath()
    {
        $dropbox = new CloudBag();
        $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $result = $dropbox->delete('/tempTest/wrongPathFolder');

        $this->assertEquals(false, $result);
    }

    public function testBreadcrumbsSuccess()
    {
        $dropbox = new CloudBag();
        $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $result = $dropbox->getBreadcrumbs('/test/testFolder/testFolder1/testFolder2');

        $expected = [
            'CloudBag' => '',
            'Test' => '/Test',
            'testFolder' => '/Test/testFolder',
            'testFolder1' => '/Test/testFolder/testFolder1',
            'testFolder2' => '/Test/testFolder/testFolder1/testFolder2',
        ];
        $this->assertEquals($expected, $result);
    }

    public function testBreadcrumbsWithEmptyPath()
    {
        $dropbox = new CloudBag();
        $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $result = $dropbox->getBreadcrumbs('');

        $this->assertEquals(['CloudBag' => ''], $result);
    }

    public function testBreadcrumbsWithWrongPath()
    {
        $dropbox = new CloudBag();
        $dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);

        $result = $dropbox->getBreadcrumbs('/wrongPath');

        $this->assertEquals(false, $result);
    }
}
