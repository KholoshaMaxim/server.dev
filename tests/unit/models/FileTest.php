<?php

namespace tests\models;

use app\models\File;

class FileTest extends \Codeception\Test\Unit
{
    public function testConstructFolderSuccess()
    {
        $file = new File($id = 'id', $name = 'name', $size = 100,
            $clientModified = '2017-10-10T11:54:44.000Z', $path = 'path');

        $this->assertEquals($id, $file->getId());
        $this->assertEquals($name, $file->getName());
        $this->assertEquals($size, $file->getSize());
        $this->assertEquals($clientModified, $file->getClientModified());
        $this->assertEquals($path, $file->getPath());
    }

    public function testConstructFileWithoutId()
    {
        $this->setExpectedException(\Exception::class);
        new File($id = '', $name = 'name', $size = 100,
            $clientModified = '2017-10-10T11:54:44.000Z', $path = 'path');
    }

    public function testConstructFileWithoutName()
    {
        $this->setExpectedException(\Exception::class);
        new File($id = 'id', $name = '', $size = 100,
            $clientModified = '2017-10-10T11:54:44.000Z', $path = 'path');
    }

    public function testConstructFileWithoutSize()
    {
        $file = new File($id = 'id', $name = 'name', $size = '',
            $clientModified = '2017-10-10T11:54:44.000Z', $path = 'path');

        $this->assertEquals($id, $file->getId());
        $this->assertEquals($name, $file->getName());
        $this->assertEquals(0, $file->getSize());
        $this->assertEquals($clientModified, $file->getClientModified());
        $this->assertEquals($path, $file->getPath());
    }

    public function testConstructFileWithoutClientModified()
    {
        $this->setExpectedException(\Exception::class);
        new File($id = 'id', $name = 'name', $size = 100,
            $clientModified = '', $path = 'path');
    }

    public function testConstructFileWithoutPath()
    {
        $this->setExpectedException(\Exception::class);
        new File($id = 'id', $name = 'name', $size = 100,
            $clientModified = '2017-10-10T11:54:44.000Z', $path = '');
    }
}
