<?php

namespace tests\models;

use app\models\Folder;

class FolderTest extends \Codeception\Test\Unit
{
    public function testConstructFolderSuccess()
    {
        $folder = new Folder($id = 'id', $name = 'name', $path = 'path');

        $this->assertEquals($id, $folder->getId());
        $this->assertEquals($name, $folder->getName());
        $this->assertEquals($path, $folder->getPath());
    }

    public function testConstructFolderWithoutId()
    {
        $this->setExpectedException(\Exception::class);
        new Folder("", "name", "path");
    }

    public function testConstructFolderWithoutName()
    {
        $this->setExpectedException(\Exception::class);
        new Folder("id", "", "path");
    }

    public function testConstructFolderWithoutPath()
    {
        $this->setExpectedException(\Exception::class);
        new Folder("id", "name", "");
    }
}
