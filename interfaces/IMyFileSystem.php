<?php

namespace app\interfaces;

interface IMyFileSystem
{
    /**
     * Get the contents of a folder
     *
     * @param $path
     * @return mixed
     */
    public function openFolder($path);

    /**
     * Delete the object
     * @param $path
     * @return mixed
     */
    public function delete($path);

    /**
     * Generate breadcrumbs
     *
     * @param $path
     * @return mixed
     */
    public function getBreadcrumbs($path);
}
