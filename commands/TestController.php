<?php

namespace app\commands;

use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use Yii;
use yii\console\Controller;

class TestController extends Controller
{
    public function actionIndex()
    {
        $app = new DropboxApp(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);
        $dropbox = new Dropbox($app);
        if (!($dropbox->getCurrentAccount()->getData()))
            exit("DropBox Authorization Error\n");

        if (!($dropbox->copy("/test/testFolder", "/tempTest/testFolder")))
            exit('Error copying folder for testing');
        if (!($dropbox->copy("/test/testFile.txt", "/tempTest/testFile.txt")))
            exit('Error copying file for testing');

        echo "Data preparation was successful\n";
    }
}

