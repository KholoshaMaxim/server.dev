<?php

namespace app\models;

class File
{
    private $id;
    private $name;
    private $size;
    private $clientModified;
    private $path;
    private $links = [];

    public function __construct($id, $name, $size, $clientModified, $path)
    {
        $this->setId($id);
        $this->setName($name);
        $this->setSize($size);
        $this->setPath($path);
        $this->setClientModified($clientModified);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @throws \Exception
     */
    private function setId($id)
    {
        if ($id == null)
            throw new \Exception("Идентификатор файла не может быть пустым");
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @throws \Exception
     */
    private function setName($name)
    {
        if ($name == null)
            throw new \Exception("Имя файла не может быть пустым");
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    private function setSize($size)
    {
        if ($size == null)
            $this->size = 0;
        else
            $this->size = $size;
    }

    /**
     * @return int
     */
    public function getClientModified()
    {
        return $this->clientModified;
    }

    /**
     * @param mixed $clientModified
     * @throws \Exception
     */
    private function setClientModified($clientModified)
    {
        if ($clientModified == null)
            throw new \Exception("Последняя модификация файла не может быть пуста");
        $this->clientModified = $clientModified;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @throws \Exception
     */
    private function setPath($path)
    {
        if ($path == null)
            throw new \Exception("Путь файла не может быть пустым");
        $this->path = $path;
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param string $key
     * @param string $link
     */
    public function setLinks($key, $link)
    {
        $this->links[$key] = $link;
    }
}
