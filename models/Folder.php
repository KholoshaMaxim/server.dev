<?php

namespace app\models;

class Folder
{
    private $id;
    private $name;
    private $path;
    private $links = [];

    public function __construct($id, $name, $path)
    {
        $this->setId($id);
        $this->setName($name);
        $this->setPath($path);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @throws \Exception
     */
    private function setId($id)
    {
        if ($id == null)
            throw new \Exception("Идентификатор папки не может быть пустым");
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @throws \Exception
     */
    private function setName($name)
    {
        if ($name == null)
            throw new \Exception("Имя папки не может быть пустым");
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @throws \Exception
     */
    private function setPath($path)
    {
        if ($path == null)
            throw new \Exception("Путь папки не может быть пустым");
        $this->path = $path;
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param string $key
     * @param string $link
     */
    public function setLinks($key, $link)
    {
        $this->links[$key] = $link;
    }
}
