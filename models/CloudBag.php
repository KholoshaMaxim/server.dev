<?php

namespace app\models;

use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;
use Kunnu\Dropbox\Exceptions;
use app\interfaces\IMyFileSystem;

class CloudBag implements IMyFileSystem
{
    private $dropbox;

    /**
     * Authorization on DropBox
     *
     * @param $clientId
     * @param $clientSecret
     * @param $accessToken
     * @return array|bool
     */
    public function auth($clientId, $clientSecret, $accessToken)
    {
        try
        {
            $app = new DropboxApp($clientId, $clientSecret, $accessToken);
            $this->dropbox = new Dropbox($app);
            $accountInfo = $this->dropbox->getCurrentAccount()->getData();
        }
        catch (Exceptions\DropboxClientException $e)
        {
            return false;
        }
        return $accountInfo;
    }

    /**
     * Get the contents of a folder on DropBox
     *
     * @param $path
     * @return array|bool
     */
    public function openFolder($path)
    {
        try
        {
            $items = $this->dropbox->listFolder($path)->getData();
        }
        catch (Exceptions\DropboxClientException $e)
        {
            return false;
        }

        $result = ['folders' => [], 'files' => []];
        foreach ($items['entries'] as $item)
        {
            if ($item['.tag'] === 'file') {
                $file = new File($item['id'], $item['name'], $item['size'],
                   $item['client_modified'] , $item['path_lower']);
                $buf['id'] = $file->getId();
                $buf['name'] = $file->getName();
                $buf['size'] = $file->getSize();
                $buf['path'] = $file->getPath();
                $buf['client_modified'] = date('Y-m-d H:i:s', strtotime($file->getClientModified()));
                $buf['links'] = $file->getLinks();
                $result['files'][] = $buf;
            } elseif ($item['.tag'] === 'folder')
                {
                    $folder = new Folder($item['id'], $item['name'], $item['path_lower']);
                    $buf['id'] = $folder->getId();
                    $buf['name'] = $folder->getName();
                    $buf['path'] = $folder->getPath();
                    $buf['links'] = $folder->getLinks();
                    $result['folders'][] = $buf;
                }
        }
        return $result;
    }

    /**
     * Delete the object on DropBox
     *
     * @param $path
     * @return bool
     */
    public function delete($path)
    {
        try
        {
            $deletedObject = $this->dropbox->delete($path);
        }
        catch (Exceptions\DropboxClientException $e)
        {
            return false;
        }

        return $deletedObject->getName();
    }

    /**
     * Generate breadcrumbs
     *
     * @param $path
     * @return array|bool
     */
    public function getBreadcrumbs($path)
    {
        $breadcrumbs = ['CloudBag' => ''];

        if ($path == '' || $path == '/')
            return $breadcrumbs;
        else
        {
            try
            {
                $file = $this->dropbox->getMetadata($path);
                $folderPaths = explode("/", $file->path_display);
                $breadcrumb = '';

                for ($i = 1; $i < count($folderPaths); $i++)
                {
                    $breadcrumb .= '/' . $folderPaths[$i];
                    $breadcrumbs[$folderPaths[$i]] = $breadcrumb;
                }
                return $breadcrumbs;
            }
            catch (Exceptions\DropboxClientException $e)
            {
                return false;
            }
        }
    }
}
