<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\CloudBag;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->response->headers->add('Access-Control-Allow-Origin', 'http://cloudbag');
        Yii::$app->response->headers->add('Access-Control-Allow-Methods', 'POST');
        Yii::$app->response->headers->add('Content-Type', 'application/json');

        if (isset($_POST['path']))
        {
            Yii::$app->response->data = json_encode($this->getData($_POST['path']));
            Yii::$app->response->send();
        }
        elseif (isset($_POST['deletePath']))
        {
            Yii::$app->response->data = json_encode($this->delete($_POST['deletePath']));
            Yii::$app->response->send();
        }
        return json_encode(['code' => '404']);
    }

    /**
     * Get the contents of the folder
     *
     * @param $path
     * @return array|bool
     */
    private function getData($path)
    {
        $dropbox = new CloudBag();
        if (!($dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'],
                Yii::$app->params['accessToken'])))
            return json_encode(['code' => '401']);

        if ($data = $dropbox->openFolder($path))
        {
            $result['code'] = '200';
            $result['data'] = $data;
            $result['breadcrumbs'] = $dropbox->getBreadcrumbs($path);
        }
        else
            $result['code'] = '404';

        return $result;
    }

    /**
     * Delete the object
     *
     * @param $path
     * @return string
     */
    private function delete($path)
    {
        $dropbox = new CloudBag();
        if (!($dropbox->auth(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'],
                Yii::$app->params['accessToken'])))
            return json_encode(['code' => '401']);

        if ($data = $dropbox->delete($path))
        {
            $result['code'] = '200';
            $result['data'] = $data;
        }
        else
            $result['code'] = '404';

        return $result;
    }
}
